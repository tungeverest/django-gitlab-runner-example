Continuously Deploying Django to DigitalOcean with Docker and GitLab
# Test run local
$ docker-compose up -d --build

Navigate to http://localhost:8000/. You should see:

{
  "hello": "world"
}

# Setup DigitalOcean Server
## Get DigitalOcean API access-token:
- In Local

$ export DIGITAL_OCEAN_ACCESS_TOKEN=[your_digital_ocean_token]

You have jq installed, then you can parse the JSON response

sudo apt install jq -y

https://developers.digitalocean.com/documentation/v2/#regions

## Next, create a new Droplet

curl -X POST \
    -H 'Content-Type: application/json' \
    -H 'Authorization: Bearer '$DIGITAL_OCEAN_ACCESS_TOKEN'' \
    -d '{"name":"django-docker","region":"sgp1","size":"s-1vcpu-2gb","image":"docker-20-04"}' \
    "https://api.digitalocean.com/v2/droplets"

Check the status:  
curl \
    -H 'Content-Type: application/json' \
    -H 'Authorization: Bearer '$DIGITAL_OCEAN_ACCESS_TOKEN'' \
    "https://api.digitalocean.com/v2/droplets?name=django-docker"

With JQ  
$ curl \
    -H 'Content-Type: application/json' \
    -H 'Authorization: Bearer '$DIGITAL_OCEAN_ACCESS_TOKEN'' \
    "https://api.digitalocean.com/v2/droplets?name=django-docker" \
  | jq '.droplets[0].status'

- Check yor email:  
Droplet Name: django-docker  
IP Address: 165.22.255.172  
Username: root  
Password: 28a302b69b0cc6135a0870dcda  

- Step 2: SSH  
ssh root@165.22.255.172

export SSH_PRIVATE_KEY='-----BEGIN OPENSSH PRIVATE KEY-----
......
......
-----END OPENSSH PRIVATE KEY-----'

ssh-add - <<< "${SSH_PRIVATE_KEY}"

- Test SSH  
sudo ssh -o StrictHostKeyChecking=no root@165.22.255.172 whoami

ssh -o StrictHostKeyChecking=no root@165.22.255.172 mkdir -p /test

## production Postgres database via DigitalOcean's Managed Databases

https://developers.digitalocean.com/documentation/v2/#databases

curl -X POST \
    -H 'Content-Type: application/json' \
    -H 'Authorization: Bearer '$DIGITAL_OCEAN_ACCESS_TOKEN'' \
    -d '{"name":"django-docker-db","region":"sgp1","engine":"pg","version":"13","size":"db-s-1vcpu-1gb","num_nodes":1}' \
    "https://api.digitalocean.com/v2/databases"

Check JQ  
curl \
    -H 'Content-Type: application/json' \
    -H 'Authorization: Bearer '$DIGITAL_OCEAN_ACCESS_TOKEN'' \
    "https://api.digitalocean.com/v2/databases?name=django-docker-db" \
  | jq '.databases[0].connection'

{
  "protocol": "postgresql",
  "uri": "postgresql://doadmin:a6bb94mcd7x5wq67@django-docker-db-do-user-9476316-0.b.db.ondigitalocean.com:25060/defaultdb?sslmode=require",
  "database": "defaultdb",
  "host": "django-docker-db-do-user-9476316-0.b.db.ondigitalocean.com",
  "port": 25060,
  "user": "doadmin",
  "password": "a6bb94mcd7x5wq67",
  "ssl": true
}

GitLab Container Registry

https://gitlab.com/tungeverest/django-gitlab-runner-example/container_registry

docker login registry.gitlab.com

## Create new dropet for Gitlab Runner
curl -X POST \
    -H 'Content-Type: application/json' \
    -H 'Authorization: Bearer '$DIGITAL_OCEAN_ACCESS_TOKEN'' \
    -d '{"name":"django-docker","region":"sgp1","size":"s-2vcpu-4gb","image":"docker-20-04"}' \
    "https://api.digitalocean.com/v2/droplets"